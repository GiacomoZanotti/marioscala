package Controller

import java.awt._

import Model.Characters.UserCharacter
import Model.Objects.Coin
import Model.Objects.GameObject
import Utils._

import scala.collection.mutable

/**
  * Created by OPeratore on 07/04/2017.
  */


class PlatformController( var coordinates: PlatformCoordinates ) {
  var objects: mutable.ListBuffer[GameObject] = coordinates.getObjects
  var coins: mutable.ListBuffer[Coin] = coordinates.getCoins
  val controllers: mutable.ListBuffer[GameController] = new mutable.ListBuffer[GameController]
  private val imgBackground1: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val imgBackground2: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val castle: Image = Utils.getImage(Res.IMG_CASTLE)
  private val start: Image = Utils.getImage(Res.START_ICON)
  private var background1PosX: Int = Constant.BACKGROUND1_START_XPOS
  private var background2PosX: Int = Constant.BACKGROUND2_START_XPOS
  private val imgCastle: Image = Utils.getImage(Res.IMG_CASTLE_FINAL)
  private val imgFlag: Image = Utils.getImage(Res.IMG_FLAG)
  private val mario: UserCharacter = coordinates.getMario


  def setBackground1PosX( xPosition: Int ) {
    this.background1PosX = xPosition
  }

  def setBackground2PosX( xPosition: Int ) {
    this.background2PosX = xPosition
  }

  def getUserCharacter: UserCharacter = {
    mario
  }

  def drawScene( graphics: Graphics ) {

    if (coordinates.getXPosition == Constant.PLATFORM_FINAL_XPOS) {
      coordinates.setXPosition(Constant.PLATFORM_START_XPOS)
      mario.setXPosition(300)
      mario.setYPosition(245)
      mario.setInvincible(false)
      background1PosX = Constant.BACKGROUND1_START_XPOS
      background2PosX = Constant.BACKGROUND2_START_XPOS
      coordinates.loadLevel(2)
    }
    this.contactWithGameObjects()
    this.contactWithEnemies()
    this.updateBackgroundPosition()
    this.updateWithWalking(graphics)

    controllers.foreach(controller => controller.drawAction(graphics))

  }


  private def updateBackgroundPosition( ) {
    if (coordinates.getXPosition >= Constant.PLATFORM_START_XPOS && coordinates.getXPosition < Constant.PLATFORM_FINAL_XPOS) {
      coordinates.setXPosition(coordinates.getXPosition + coordinates.getMov)
      this.background1PosX = this.background1PosX - coordinates.getMov
      this.background2PosX = this.background2PosX - coordinates.getMov
    }
    if (this.background1PosX == -PlatformController.FLIPPING_CONSTANT) {
      this.background1PosX = PlatformController.FLIPPING_CONSTANT
    }
    else if (this.background2PosX == -PlatformController.FLIPPING_CONSTANT) {
      this.background2PosX = PlatformController.FLIPPING_CONSTANT
    }
    else if (this.background1PosX == PlatformController.FLIPPING_CONSTANT) {
      this.background1PosX = -PlatformController.FLIPPING_CONSTANT
    }
    else if (this.background2PosX == PlatformController.FLIPPING_CONSTANT) {
      this.background2PosX = -PlatformController.FLIPPING_CONSTANT
    }

  }

  private def contactWithGameObjects( ) {


    coordinates.getObjects.foreach(gameObject => controllers.foreach(controller => controller.interaction(gameObject)))

    coordinates.getCoins.foreach(coin => controllers.foreach(controller => controller.interaction(coin)))


  }


  private def contactWithEnemies( ) {

    coordinates.getEnemyCharacters.foreach(enemy => controllers foreach (controller => controller.interaction(enemy)))
  }

  private def updateWithWalking( graphics: Graphics ) {
    this.drawSteadyObject(graphics)
    if (coordinates.getXPosition >= Constant.PLATFORM_START_XPOS && coordinates.getXPosition <= Constant.PLATFORM_FINAL_XPOS) {
      for (gameObject <- coordinates.getObjects) {
        graphics.drawImage(gameObject.getImageGameObject, gameObject.getXPosition, gameObject.getYPosition, null)
        moveObject(gameObject)
      }
      for (coin <- coordinates.getCoins) {
        graphics.drawImage(coin.imageOnMovement, coin.getXPosition, coin.getYPosition, null)
        moveObject(coin)
      }

      coordinates.getEnemyCharacters.foreach(enemy => enemy.move())


    }

  }

  private def drawSteadyObject( graphics: Graphics ) {
    graphics.drawImage(this.imgBackground1, this.background1PosX, PlatformController.BACKGROUND_1_START_Y, null)
    graphics.drawImage(this.imgBackground2, this.background2PosX, PlatformController.BACKGROUND_2_START_Y, null)
    graphics.drawImage(this.castle, PlatformController.CASTLE_START_OFFSET_X - coordinates.getXPosition, PlatformController.CASTLE_START_OFFSET_Y, null)
    graphics.drawImage(this.start, PlatformController.FLAG_START_OFFSET_X - coordinates.getXPosition, PlatformController.FLAG_START_OFFSET_Y, null)
    graphics.drawImage(this.imgFlag, PlatformController.FLAG_X_POS - coordinates.getXPosition, PlatformController.FLAG_Y_POS, null)
    graphics.drawImage(this.imgCastle, PlatformController.CASTLE_X_POS - coordinates.getXPosition, PlatformController.CASTLE_Y_POS, null)
  }

  private def moveObject( gameObject: GameObject ) {
    if (coordinates.getXPosition >= 0) {
      gameObject.setXPosition(gameObject.getXPosition - coordinates.getMov)
    }
  }

  def addCharacterController( controller: GameController ) {
    this.controllers.append(controller)
  }

}

object PlatformController {
  private val FLAG_X_POS: Int = 4650
  private val CASTLE_X_POS: Int = 4850
  private val FLAG_Y_POS: Int = 115
  private val CASTLE_Y_POS: Int = 145
  private val CASTLE_START_OFFSET_X: Int = 10
  private val FLAG_START_OFFSET_X: Int = 220
  private val FLAG_START_OFFSET_Y: Int = 234
  private val CASTLE_START_OFFSET_Y: Int = 95
  private val BACKGROUND_1_START_Y: Int = 0
  private val BACKGROUND_2_START_Y:Int=0
  private val FLIPPING_CONSTANT:Int=800
}




