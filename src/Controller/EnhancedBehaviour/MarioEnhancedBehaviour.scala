package Controller.EnhancedBehaviour

import Controller.UserCharacterController
import Model.Objects.GameObject
import Utils.Constant

/**
  * Created by OPeratore on 10/04/2017.
  */
trait MarioEnhancedBehaviour extends UserCharacterController {

  def becomeInvincible( ): Unit = this.coinCounter match {
    case Constant.INVINCIBLE_COIN_COUNTER => this.mario.setInvincible(true)

    case _ =>



  }


  override def hitCoin( coin: GameObject ): Boolean = {
    this.becomeInvincible()
    this.mario.hitBack(coin) || this.mario.hitAbove(coin) || this.mario.hitAhead(coin) || this.mario.hitBelow(coin)

  }


}
