package Controller

import Model._
import Model.Characters.EnemyCharacter
import Model.Objects.Coin
import java.awt._
import java.util.List

import scala.collection.mutable

/**
  * Created by OPeratore on 19/03/2017.
  */
class EnemyCharacterController( val platformCoordinates: PlatformCoordinates ) extends GameController {


  def interaction( gameElement: GameElement ) {

    platformCoordinates.getEnemyCharacters.foreach(enemy => if (!(enemy == gameElement) && !gameElement.isInstanceOf[Coin] && enemy.isNearby(gameElement)) {
      contact(enemy, gameElement)
    })
  }

  private def contact( enemyCharacter: EnemyCharacter, gameElement: GameElement ) {
    if (enemyCharacter.hitAhead(gameElement) && enemyCharacter.isToRight) {
      enemyCharacter.setToRight(false)
    }
    else if (enemyCharacter.hitBack(gameElement) && !enemyCharacter.isToRight) {
      enemyCharacter.setToRight(true)
    }
  }

  def drawAction( graphics: Graphics ) {
    platformCoordinates.getEnemyCharacters.foreach(enemyCharacter => if (enemyCharacter.isAlive) {
      graphics.drawImage(enemyCharacter.walk, enemyCharacter.getXPosition, enemyCharacter.getYPosition, null)
    } else {
      graphics.drawImage(enemyCharacter.deadImage, enemyCharacter.getXPosition, enemyCharacter.getYPosition + enemyCharacter.getDeadOffset, null)
    })
  }
}