package Controller

import Model.GameElement
import java.awt._

/**
  * Created by OPeratore on 04/04/2017.
  */
trait GameController {
  def interaction(gameElement: GameElement)


  def drawAction( graphics: Graphics )

}