package Controller

import java.util

import Model._

import Utils._
import View.Audio
import java.awt._

import Model.Characters.{EnemyCharacter, UserCharacter}
import Model.Objects.{Coin, GameObject}


/**
  * Created by OPeratore on 19/03/2017.
  */
object UserCharacterController {
  protected val MARIO_OFFSET_Y_INITIAL: Int = 243
  protected val FLOOR_OFFSET_Y_INITIAL: Int = 293
  protected val JUMPING_LIMIT: Int = 42
}

abstract class UserCharacterController( var platformCoordinates: PlatformCoordinates ) extends GameController {
  protected var mario: UserCharacter = platformCoordinates.getMario
  protected var coinCounter: Int = 0
  def interaction( gameElement: GameElement ) {

    gameElement match {
      case coin: Coin if this.hitCoin(coin) =>
        this.coinCounter = this.coinCounter + Constant.INCREMENT
        Audio.playSound(Res.AUDIO_MONEY)
        platformCoordinates.getCoins -= coin
      case _ =>
    }

    if (this.mario.isNearby(gameElement)) {
      this.contact(gameElement)
    }
  }


  def drawAction( graphics: Graphics ) {

    if (mario.isJumping) {
      graphics.drawImage(this.doJump(), mario.getXPosition, mario.getYPosition, null)
    }
    else if (mario.isAlive) {
      graphics.drawImage(mario.walk, mario.getXPosition, mario.getYPosition, null)
    }
    if (!mario.isAlive) {
      graphics.drawImage(mario.deadImage, mario.getXPosition, mario.getYPosition, null)
    }
  }

  protected def doJump( ): Image = {
    var string: String = null
    this.mario.increaseJumping()
    if (this.mario.getJumpingExtent < UserCharacterController.JUMPING_LIMIT) {
      if (mario.getYPosition > this.platformCoordinates.getHeightLimit) mario.setYPosition(mario.getYPosition - Constant.MARIO_OFFSET_Y)
      else this.mario.setJumpingExtent(UserCharacterController.JUMPING_LIMIT)
      string = if (mario.isToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else if (mario.getYPosition + mario.getHeight < this.platformCoordinates.getFloorOffsetY) {
      mario.setYPosition(mario.getYPosition + Constant.INCREMENT)
      string = if (mario.isToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else {
      string = if (mario.isToRight) Res.IMG_MARIO_ACTIVE_DX
      else Res.IMG_MARIO_ACTIVE_SX
      mario.setJumping(false)
      this.mario.setJumpingExtent(Constant.MARIO_INITIAL_JUMPING_EXTENT)
    }
    Utils.getImage(string)
  }

  protected def contact( gameElement: GameElement ) {

    gameElement match {
      case gameObject: GameObject => contactGameObject(gameObject)
      case enemy: EnemyCharacter => contactEnemyCharacter(enemy)

    }

  }

  protected def contactGameObject( gameElement: GameElement ) {
    if (!gameElement.isInstanceOf[Coin] && mario.hitAhead(gameElement) && mario.isToRight || mario.hitBack(gameElement) && !mario.isToRight) {
      this.platformCoordinates.setMov(Constant.PLATFORM_NOT_MOV)
      mario.setMoving(false)
    }
    if (!gameElement.isInstanceOf[Coin] && mario.hitBelow(gameElement) && !gameElement.isInstanceOf[Coin] && mario.isJumping) {
      this.platformCoordinates.setFloorOffsetY(gameElement.getYPosition)
    }
    else if (!gameElement.isInstanceOf[Coin] && !mario.hitBelow(gameElement)) {
      this.platformCoordinates.setFloorOffsetY(UserCharacterController.FLOOR_OFFSET_Y_INITIAL)
      if (!mario.isJumping) {
        mario.setYPosition(UserCharacterController.MARIO_OFFSET_Y_INITIAL)
      }
      if (!gameElement.isInstanceOf[Coin] && mario.hitAbove(gameElement)) {
        this.platformCoordinates.setHeightLimit(gameElement.getYPosition + gameElement.getHeight) // the new sky goes below the object
      }
      else if (!gameElement.isInstanceOf[Coin] && !mario.hitAbove(gameElement) && !mario.isJumping) {
        this.platformCoordinates.setHeightLimit(Constant.INITIAL_SKY) // initial sky
      }
    }
  }

  def contactEnemyCharacter( gameElement: GameElement ) {
    val enemyCharacter: EnemyCharacter = gameElement.asInstanceOf[EnemyCharacter]
    if (mario.hitAhead(enemyCharacter) || mario.hitBack(enemyCharacter)) {
      if (enemyCharacter.isAlive && !mario.isInvincible) {
        mario.setMoving(false)
        mario.setAlive(false)
      }
      else if (enemyCharacter.isAlive && mario.isInvincible) {
        mario.setAlive(true)

      }

    }
    else if (mario.hitBelow(enemyCharacter)) {
      enemyCharacter.setMoving(false)
      enemyCharacter.setAlive(false)
    }
  }

  def hitCoin( coin: GameObject ): Boolean = {

    this.mario.hitBack(coin) || this.mario.hitAbove(coin) || this.mario.hitAhead(coin) || this.mario.hitBelow(coin)
  }


}

case class MarioController( platfrmCoordinates: PlatformCoordinates ) extends UserCharacterController(platfrmCoordinates)

