package Controller

import Model.Characters.EnemyCharacter
import Model.Characters.Mario
import Model.Characters.UserCharacter
import Model.Objects.{Coin, GameObject}
import Utils.CharactersUtils
import Utils.ObjectsUtils

import scala.collection.mutable

/**
  * Created by OPeratore on 07/04/2017.
  */
object PlatformCoordinates {
  private var ourInstance: PlatformCoordinates = _

  def getInstance: PlatformCoordinates = {
    if (ourInstance == null) {
      ourInstance = new PlatformCoordinates
    }
    ourInstance
  }
}

class PlatformCoordinates private( ) {
  private var mov: Int = 0
  private var xPosition: Int = -1
  private var floorOffsetY: Int = 293
  private var heightLimit: Int = 0
  private var objects: mutable.ListBuffer[GameObject] = ObjectsUtils.loadObjectsLevel1
  private val mario: UserCharacter = new Mario(300, 245)
  private var coins: mutable.ListBuffer[Coin] = ObjectsUtils.loadCoinsLevel1
  private var enemies: mutable.ListBuffer[EnemyCharacter] = CharactersUtils.getListOfEnemiesLevel1

  def loadLevel( levelNumber: Int ): Unit = levelNumber match {

    case 2 =>
      this.objects.clear()
      this.coins.clear()
      this.enemies.clear()
      this.objects = ObjectsUtils.loadGameObjectsLevel2
      this.coins = ObjectsUtils.loadCoinsLevel2
      this.enemies = CharactersUtils.getLisOfEnemiesLevel2

  }

  def getFloorOffsetY: Int = {
    floorOffsetY
  }

  def getHeightLimit: Int = {
    heightLimit
  }

  def getXPosition: Int = {
    xPosition
  }

  def setFloorOffsetY( floorOffsetY: Int ) {
    this.floorOffsetY = floorOffsetY
  }

  def setHeightLimit( heightLimit: Int ) {
    this.heightLimit = heightLimit
  }

  def setXPosition( xPos: Int ) {
    this.xPosition = xPos
  }

  def setMov( mov: Int ) {
    this.mov = mov
  }

  def getMario: UserCharacter = {
    mario
  }

  def getObjects: mutable.ListBuffer[GameObject] = {
    objects
  }

  def getCoins: mutable.ListBuffer[Coin] = {
    coins
  }


  private[Controller] def getMov: Int = {
    mov
  }


  private[Controller] def getEnemyCharacters: mutable.ListBuffer[EnemyCharacter] = {
    enemies
  }
}