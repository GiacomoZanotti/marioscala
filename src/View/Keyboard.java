package View;

import Controller.PlatformCoordinates;
import Controller.PlatformController;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class Keyboard implements KeyListener {

    private PlatformCoordinates coordinates;
    private PlatformController platformController;

    public Keyboard(PlatformController platformController) {
        this.coordinates = PlatformCoordinates.getInstance();
        this.platformController = platformController;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (platformController.getUserCharacter().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (coordinates.getXPosition() == -1) {
                    coordinates.setXPosition(0);
                    platformController.setBackground1PosX(-50);
                    platformController.setBackground2PosX(750);

                }
                platformController.getUserCharacter().setMoving(true);
                platformController.getUserCharacter().setToRight(true);
                coordinates.setMov(1); // si muove verso sinistra


            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (coordinates.getXPosition() == 4601) {
                    coordinates.setXPosition(4600);
                    platformController.setBackground1PosX(-50);
                    platformController.setBackground2PosX(750);

                }

                platformController.getUserCharacter().setMoving(true);
                platformController.getUserCharacter().setToRight(false);
                coordinates.setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                platformController.getUserCharacter().setJumping(true);

                Audio.playSound("/Resources/audio/jump.wav");
            }


        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        platformController.getUserCharacter().setMoving(false);
        coordinates.setMov(0);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

}
