package View;



import Controller.PlatformController;

import java.awt.*;


import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Platform extends JPanel {



    private PlatformController platformController;


    public Platform(PlatformController platformController) {
        this.platformController=platformController;

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard(platformController));

    }




    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);


        this.platformController.drawScene(graphics);

    }
}



