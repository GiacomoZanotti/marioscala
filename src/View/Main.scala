package View

import javax.swing.JFrame

import Controller.EnhancedBehaviour.MarioEnhancedBehaviour
import Controller._


object Main {
  private val WINDOW_WIDTH: Int = 700
  private val WINDOW_HEIGHT: Int = 360
  private val WINDOW_TITLE: String = "Super Mario"

  def main( args: Array[String] ) {
    val window: JFrame = new JFrame(WINDOW_TITLE)
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT)
    window.setLocationRelativeTo(null)
    window.setResizable(true)
    window.setAlwaysOnTop(true)

    val coordinates: PlatformCoordinates = PlatformCoordinates.getInstance
    val platformController: PlatformController = new PlatformController(coordinates)
    val controllerMario: GameController = new MarioController(coordinates) with MarioEnhancedBehaviour
    val controllerEnemies: GameController = new EnemyCharacterController(coordinates)
    platformController.addCharacterController(controllerMario)
    platformController.addCharacterController(controllerEnemies)
    val platform: Platform = new Platform(platformController)
    window.setContentPane(platform)
    window.setVisible(true)
    val timer: Thread = new Thread(new Refresh(platform))
    timer.start()
  }
}