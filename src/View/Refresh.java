package View;

public class Refresh implements Runnable {

    private Platform platform;
    public Refresh(Platform platform){
        this.platform=platform;
    }
    private final int PAUSE = 3;

    public void run() {
        while (true) {
            platform.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
