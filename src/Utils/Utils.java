package Utils;


import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Roberto Casadei
 */

public class Utils {
    private Utils(){}//Item 4: noninstantiability of utility class;
    public static URL getResource(String path){
        return Utils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }


}
