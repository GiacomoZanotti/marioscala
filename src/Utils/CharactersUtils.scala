package Utils

import Model.Characters._


import scala.collection.mutable

/**
  * Created by OPeratore on 06/04/2017.
  */
object CharactersUtils {
  private val mario: UserCharacter = new Mario(300, 245)
  private val turtle1: EnemyCharacter = new Turtle(1000, 243)
  private val mushroom: EnemyCharacter = new Mushroom(800, 263)
  private val turtle2: EnemyCharacter = new Turtle(2600, 243)
  private val turtle3: EnemyCharacter = new Turtle(600, 243)
  private val turtle4: EnemyCharacter = new Turtle(3000, 243)
  private val enemies: mutable.ListBuffer[EnemyCharacter] = new mutable.ListBuffer[EnemyCharacter]

  def getListOfEnemiesLevel1: mutable.ListBuffer[EnemyCharacter] = {
    enemies.append(turtle1)
    enemies.append(turtle2)
    enemies.append(turtle3)
    enemies.append(turtle4)
    enemies.append(mushroom)
     enemies
  }

  def getLisOfEnemiesLevel2:mutable.ListBuffer[EnemyCharacter]={
    val enemies_level2: mutable.ListBuffer[EnemyCharacter] = new mutable.ListBuffer[EnemyCharacter]
    enemies_level2.append(new Mushroom(900,263))
    enemies_level2.append(new Turtle(2000,243))
    enemies_level2
  }

  def getMario: UserCharacter = {
     mario
  }

}

