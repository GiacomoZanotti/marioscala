package Utils

/**
  * Created by OPeratore on 10/04/2017.
  */
object Constant {
  val PROXIMITY_MARGIN: Int = 10
  val PLATFORM_NOT_MOV: Int = 0
  val INITIAL_SKY: Int = 0
  val MARIO_INITIAL_JUMPING_EXTENT: Int = 0
  val MARIO_OFFSET_Y: Int = 4
  val INCREMENT: Int = 1
  val BACKGROUND1_START_XPOS: Int = -50
  val BACKGROUND2_START_XPOS: Int = 750
  val PLATFORM_START_XPOS: Int = 0
  val PLATFORM_FINAL_XPOS: Int = 4600
  val ENEMY_OFFSET_X_RIGHT: Int = 1
  val ENEMY_OFFSET_X_LEFT: Int = -1
  val INVINCIBLE_COIN_COUNTER: Int = 4

}
