package Utils

import Model.Objects.Block
import Model.Objects.Coin
import Model.Objects.GameObject
import Model.Objects.Tunnel


import scala.collection.mutable

/**
  * Created by OPeratore on 15/03/2017.
  */
object ObjectsUtils {
  private val gameObjectsLevel1: mutable.ListBuffer[GameObject] = new mutable.ListBuffer[GameObject]
  private val gameObjectsLevel2: mutable.ListBuffer[GameObject] = new mutable.ListBuffer[GameObject]
  private val coinsLevel1:mutable.ListBuffer[Coin]=new mutable.ListBuffer[Coin]
  private val coinsLevel2:mutable.ListBuffer[Coin]=new mutable.ListBuffer[Coin]

  private val tunnel1: GameObject = new Tunnel(600, 230)
  private val tunnel2: GameObject = new Tunnel(1000, 230)
  private val tunnel3: GameObject = new Tunnel(1600, 230)
  private val tunnel4: GameObject = new Tunnel(1900, 230)
  private val tunnel5: GameObject = new Tunnel(2500, 230)
  private val tunnel6: GameObject = new Tunnel(3000, 230)
  private val tunnel7: GameObject = new Tunnel(3800, 230)
  private val tunnel8: GameObject = new Tunnel(4500, 230)
  private val block1: GameObject = new Block(400, 180)
  private val block2: GameObject = new Block(1200, 180)
  private val block3: GameObject = new Block(1270, 170)
  private val block4: GameObject = new Block(1340, 160)
  private val block5: GameObject = new Block(2000, 180)
  private val block6: GameObject = new Block(2600, 160)
  private val block7: GameObject = new Block(2650, 180)
  private val block8: GameObject = new Block(3500, 160)
  private val block9: GameObject = new Block(3550, 140)
  private val block10: GameObject = new Block(4000, 170)
  private val piece1: Coin = new Coin(402, 145)
  private val piece2: Coin = new Coin(1202, 140)
  private val piece3: Coin = new Coin(1272, 95)
  private val piece4: Coin = new Coin(1342, 40)
  private val piece5: Coin = new Coin(1650, 145)
  private val piece6: Coin = new Coin(2650, 145)
  private val piece7: Coin = new Coin(3000, 135)
  private val piece8: Coin = new Coin(3400, 125)
  private val piece9: Coin = new Coin(4200, 145)
  private val piece10: Coin = new Coin(4600, 40)

  def loadObjectsLevel1: mutable.ListBuffer[GameObject] = {
    gameObjectsLevel1.append(tunnel1)
    gameObjectsLevel1.append(tunnel2)
    gameObjectsLevel1.append(tunnel3)
    gameObjectsLevel1.append(tunnel4)
    gameObjectsLevel1.append(tunnel5)
    gameObjectsLevel1.append(tunnel6)
    gameObjectsLevel1.append(tunnel7)
    gameObjectsLevel1.append(tunnel8)
    gameObjectsLevel1.append(block1)
    gameObjectsLevel1.append(block2)
    gameObjectsLevel1.append(block3)
    gameObjectsLevel1.append(block4)
    gameObjectsLevel1.append(block5)
    gameObjectsLevel1.append(block6)
    gameObjectsLevel1.append(block7)
    gameObjectsLevel1.append(block8)
    gameObjectsLevel1.append(block9)
    gameObjectsLevel1.append(block10)

     gameObjectsLevel1
  }
  def loadCoinsLevel1():mutable.ListBuffer[Coin]={
    coinsLevel1.append(piece1)
    coinsLevel1.append(piece2)
    coinsLevel1.append(piece3)
    coinsLevel1.append(piece4)
    coinsLevel1.append(piece5)
    coinsLevel1.append(piece6)
    coinsLevel1.append(piece7)
    coinsLevel1.append(piece8)
    coinsLevel1.append(piece9)
    coinsLevel1.append(piece10)
    coinsLevel1

  }
  def loadCoinsLevel2():mutable.ListBuffer[Coin]={
    coinsLevel2.append( new Coin(1300, 145))
    coinsLevel2.append(new Coin(1600, 140))
    coinsLevel2.append( new Coin(1900, 95))
    coinsLevel2

  }
  def loadGameObjectsLevel2():mutable.ListBuffer[GameObject]={
    gameObjectsLevel2.append(new Block(400, 180))
    gameObjectsLevel2.append(new Block(1300, 180))
    gameObjectsLevel2.append(new Block(1600, 170))
    gameObjectsLevel2.append(new Block(1900, 160))
    gameObjectsLevel2.append(new Block(1370, 160))
    gameObjectsLevel2.append(new Tunnel(600, 230))
    gameObjectsLevel2.append(new Tunnel(1000, 230))
    gameObjectsLevel2.append(new Tunnel(1600, 230))
    gameObjectsLevel2.append(new Tunnel(1900, 230))
    gameObjectsLevel2.append(new Tunnel(3000, 230))
    gameObjectsLevel2.append(tunnel6)
    gameObjectsLevel2.append(tunnel7)
    gameObjectsLevel2.append(tunnel8)


    gameObjectsLevel2
  }

}

class ObjectsUtils private( ) {
}