package Model.Characters

import java.awt.Image

import Model.Abstracts.AbstractEnemy
import Utils.{Res, Utils}

object Turtle {
  private val WIDTH: Int = 43
  private val HEIGHT: Int = 50
  private val TURTLE_FREQUENCY: Int = 1
  private val TURTLE_DEAD_OFFSET_Y: Int = 30
}

class Turtle( val X: Int, val Y: Int ) extends AbstractEnemy(X, Y, Turtle.WIDTH, Turtle.HEIGHT) {
  super.setToRight(true)
  super.setMoving(true)
  val enemyCharacter: Thread = new Thread(this)
  enemyCharacter.start()

  def deadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)

  def getFrequency: Int = Turtle.TURTLE_FREQUENCY

  def getImageName: String = Res.IMGP_CHARACTER_TURTLE

  def getDeadOffset: Int = Turtle.TURTLE_DEAD_OFFSET_Y
}