package Model.Characters

import java.awt.Image

import Model.Abstracts.BasicCharacter
import Model.Objects.GameObject
import Utils.{Res, Utils}

object Mario {
  private val MARIO_FREQUENCY: Int = 25
  private val WIDTH: Int = 28
  private val HEIGHT: Int = 50
  def apply():UserCharacter=new Mario(300,245)

}

 class Mario( xPosition: Int, yPosition: Int ) extends BasicCharacter(xPosition, yPosition, Mario.WIDTH, Mario.HEIGHT) with UserCharacter {

  private var invincible: Boolean = false
  private var jumping: Boolean = false
  private var jumpingExtent:Int=0

  def getJumpingExtent:Int={
    jumpingExtent
  }

  def increaseJumping():Unit={
    jumpingExtent=jumpingExtent+1
  }

  def setJumpingExtent(jumpingExtent:Int): Unit ={
    this.jumpingExtent=jumpingExtent
  }

  def isJumping: Boolean = {
    jumping
  }

  def setJumping( jumping: Boolean ) {
    this.jumping = jumping
  }

  def isInvincible: Boolean = {
    invincible
  }

  def setInvincible( condition: Boolean ) {
    this.invincible = condition
  }

  def deadImage: Image = {
    null
  }

  def getFrequency: Int = {
    Mario.MARIO_FREQUENCY
  }

  def getImageName: String = {
    Res.IMGP_CHARACTER_MARIO
  }

 }