package Model.Characters

/**
  * Created by OPeratore on 13/03/2017.
  */
trait EnemyCharacter extends GameCharacter {
  def move( )

  def getDeadOffset: Int
}
