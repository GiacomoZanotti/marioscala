package Model.Characters

/**
  * Created by OPeratore on 13/03/2017.
  */
trait UserCharacter extends GameCharacter {
  def isJumping: Boolean

  def setJumping( jumping: Boolean )

  def isInvincible: Boolean

  def setInvincible( condition: Boolean )

  def getJumpingExtent: Int

  def increaseJumping( ): Unit

  def setJumpingExtent(jumpingExtent:Int): Unit
}
