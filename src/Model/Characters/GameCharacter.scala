package Model.Characters

import java.awt.Image

import Model.GameElement
import Utils.Constant

/**
  * Created by OPeratore on 13/03/2017.
  */
trait GameCharacter extends GameElement {

  def hitAbove( gameElement: GameElement ): Boolean = {
    !(this.getXPosition +  this.getWidth < gameElement.getXPosition + 5 || this.getXPosition > gameElement.getXPosition + gameElement.getWidth - 5 || this.getYPosition < gameElement.getYPosition + gameElement.getHeight || this.getYPosition > gameElement.getYPosition + gameElement.getHeight + 5)
  }

  def hitAhead( gameElement: GameElement ): Boolean = {
    this.isToRight && !(this.getXPosition + this.getWidth < gameElement.getXPosition || this.getXPosition + this.getWidth > gameElement.getXPosition + 5 || this.getYPosition + this.getHeight <= gameElement.getYPosition || this.getYPosition >= gameElement.getYPosition + gameElement.getHeight)
  }

  def hitBack( gameElement: GameElement ): Boolean = {
    !(this.getXPosition > gameElement.getXPosition + gameElement.getWidth || this.getXPosition + this.getWidth < gameElement.getXPosition + gameElement.getWidth - 5 || this.getYPosition + this.getWidth <= gameElement.getYPosition || this.getYPosition >= gameElement.getYPosition + gameElement.getHeight)
  }

  def hitBelow( gameElement: GameElement ): Boolean = {
    !(this.getXPosition + this.getWidth < gameElement.getXPosition || this.getXPosition > gameElement.getXPosition + gameElement.getWidth || this.getYPosition + this.getHeight < gameElement.getYPosition || this.getYPosition + this.getHeight > gameElement.getYPosition)
  }


  def isNearby( gameElement: GameElement ): Boolean = {
    (this.getXPosition > gameElement.getXPosition - Constant.PROXIMITY_MARGIN && this.getXPosition < gameElement.getXPosition + gameElement.getWidth + Constant.PROXIMITY_MARGIN) || (this.getXPosition + this.getWidth > gameElement.getXPosition - Constant.PROXIMITY_MARGIN && this.getXPosition + this.getWidth < gameElement.getXPosition + gameElement.getWidth + Constant.PROXIMITY_MARGIN)
  }

  def walk: Image

  def isAlive: Boolean

  def isMoving: Boolean

  def setAlive( alive: Boolean )

  def setXPosition( xPosition: Int )

  def setYPosition( yPosition: Int )

  def deadImage: Image

  def isToRight: Boolean

  def setMoving( moving: Boolean )

  def setToRight( toRight: Boolean )
}
