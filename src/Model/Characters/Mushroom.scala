package Model.Characters

import java.awt.Image

import Model.Abstracts.AbstractEnemy
import Utils.{Res, Utils}

object Mushroom {
  private val WIDTH: Int = 27
  private val HEIGHT: Int = 30
  private val MUSHROOM_FREQUENCY: Int = 45
  private val MUSHROOM_DEAD_OFFSET_Y: Int = 20
}

class Mushroom(  xPosition: Int,  yPosition: Int ) extends AbstractEnemy(xPosition, yPosition, Mushroom.WIDTH, Mushroom.HEIGHT) {
  this.setToRight(true)
  this.setMoving(true)
  val enemyCharacter: Thread = new Thread(this)
  enemyCharacter.start()

  def deadImage: Image = {
     Utils.getImage(if (this.isToRight) Res.IMG_MUSHROOM_DEAD_DX
    else Res.IMG_MUSHROOM_DEAD_SX)
  }

  def getFrequency: Int = {
     Mushroom.MUSHROOM_FREQUENCY
  }

  def getImageName: String = {
     Res.IMGP_CHARACTER_MUSHROOM
  }

  def getDeadOffset: Int = {
     Mushroom.MUSHROOM_DEAD_OFFSET_Y
  }
}