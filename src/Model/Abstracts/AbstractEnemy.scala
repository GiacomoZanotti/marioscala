package Model.Abstracts

import java.awt._

import Model.Characters.EnemyCharacter
import Utils.Constant

/**
  * Created by OPeratore on 13/03/2017.
  */
abstract class AbstractEnemy( xPosition: Int, yPosition: Int,  width: Int,  height: Int ) extends BasicCharacter(xPosition, yPosition, width, height) with Runnable with EnemyCharacter {
  private var offsetX: Int = 0
  final private val PAUSE: Int = 100

  def move( ) {
    this.offsetX = if (isToRight) Constant.ENEMY_OFFSET_X_RIGHT
    else Constant.ENEMY_OFFSET_X_LEFT
    this.setXPosition(this.getXPosition + this.offsetX)
  }

  def run( ) {
    while (this.isAlive) {

        this.move()
        try {
          Thread.sleep(PAUSE)
        }
        catch {
          case e: InterruptedException =>
            System.err.println("something went wrong with thread Mush" + e)
        }
      }
    }




  def deadImage: Image
}