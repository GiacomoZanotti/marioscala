package Model.Abstracts

import java.awt.Image

import Model.Characters.GameCharacter
import Utils.{Res, Utils}



abstract class BasicCharacter( var xPosition: Int, var yPosition: Int, var width: Int, var height: Int ) extends GameCharacter {



  protected var moving: Boolean = false
  protected var toRight: Boolean = true
  protected var counter: Int = 0
  protected var alive: Boolean = true

  def getXPosition: Int = {
     xPosition
  }

  def getYPosition: Int = {
     yPosition
  }

  def getWidth: Int = {
     width
  }

  def getHeight: Int = {
     height
  }

  def isAlive: Boolean = {
     alive
  }

  def isToRight: Boolean = {
     toRight
  }

  override def isMoving: Boolean =  {
    moving
  }

  def setAlive( alive: Boolean ) {
    this.alive = alive
  }

  def setXPosition( xPosition: Int ) {
    this.xPosition = xPosition
  }

  def setYPosition( yPosition: Int ) {
    this.yPosition = yPosition
  }

  def setMoving( moving: Boolean ) {
    this.moving = moving
  }

  def setToRight( toRight: Boolean ) {
    this.toRight = toRight
  }

  def walk: Image = {
    val name: String = this.getImageName
    val str: String = Res.IMG_BASE + name + (if (!this.moving || {
      this.counter += 1; this.counter
    } % this.getFrequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (this.toRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
     Utils.getImage(str)
  }





  def deadImage: Image

  def getFrequency: Int

  def getImageName: String
}