package Model

/**
  * Created by OPeratore on 13/03/2017.
  */
trait GameElement {
  def getWidth: Int

  def getHeight: Int

  def getXPosition: Int

  def getYPosition: Int
}