package Model.Objects

import java.awt._

import Model.GameElement
import Utils.{Res, Utils}

class GameObject( var xPosition: Int, var yPosition: Int, var width: Int, var height: Int ) extends GameElement {
  protected var imageGameObject: Image = _


  def getWidth: Int = {
    width
  }

  def getHeight: Int = {
    height
  }

  def getXPosition: Int = {
    xPosition
  }

  def getYPosition: Int = {
    yPosition
  }

  def getImageGameObject: Image = {
    imageGameObject
  }

  def setXPosition( xPosition: Int ): Unit = {
    this.xPosition = xPosition
  }

}

case class Block( x: Int, y: Int ) extends GameObject(x, y, Block.WIDTH, Block.HEIGHT) {
  this.imageGameObject = Utils.getImage(Res.IMG_BLOCK)

}

case class Tunnel( x: Int, y: Int ) extends GameObject(x, y, Tunnel.WIDTH, Tunnel.HEIGHT) {
  this.imageGameObject = Utils.getImage(Res.IMG_TUNNEL)

}

object Block {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30


}


object Tunnel {
  private val WIDTH: Int = 43
  private val HEIGHT: Int = 65
}

