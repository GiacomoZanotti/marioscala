package Model.Objects

import java.awt._

import Utils.{Res, Utils}

object Coin  {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
  private val FLIP_FREQUENCY: Int = 100
  private val PAUSE: Int = 15


}

class Coin( var x: Int, var y: Int ) extends GameObject(x, y, Coin.WIDTH, Coin.HEIGHT) with Runnable {
  this.imageGameObject = Utils.getImage(Res.IMG_PIECE1)
  private var counter: Int = 0

  def imageOnMovement: Image = {
     Utils.getImage(if ( {
      this.counter += 1
      this.counter
    } % Coin.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  def run( ) {
    this.imageOnMovement
    try {
      Thread.sleep(Coin.PAUSE)
    }
    catch {
      case e: InterruptedException => {
        System.err.println("something went wrong" + e)
      }
    }
  }
}